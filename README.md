# agentk-agent

## Overview
---
This project installs the GitLab Agent for Kubernetes in the cluster & links it to GitLab.

This is a part of the overall set of projects which will setup everything & configure the GitLab Agent for Kubernetes.

The overall order of execution is:
1. PreRequisites - [agentk-prereq](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-prereq)
2. Create Service Account - [agentk-service-account](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-service-account)
3. Create Network - [agentk-network](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-network)
4. Create Kubernetes Cluster - [agentk-cluster](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-cluster)
5. Configure Cluster Role Bindings - [agentk-cluster-role-binding](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-cluster-role-binding)
6. Create Agent Registration Project - [agentk-registration](https://gitlab.com/sandlin/agentk-registration)
7. Install GitLab Agent - [agentk-agent](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-agent)
9. Configure Agent Registration Project
   1. Modify the [config.yml](https://gitlab.com/sandlin/agentk-registration/-/blob/groot/.gitlab/agents/gitlab-agent/config.yaml)
   1. Set `gitops.manifest_projects.id` to point to your `Cluster Services Management Project`
   2. Set `ci_access.groups.id` to your org.
8. Create Services Management Project - [agentk-services](https://gitlab.com/sandlin/agentk-services)
   1. - This was created by following [Manage Cluster Applications](https://docs.gitlab.com/ee/user/clusters/management_project_template.html#create-a-project-based-on-the-cluster-management-project-template)
   2. Modify the [.gitlab-ci.yml](https://gitlab.com/sandlin/agentk-services/-/blob/groot/.gitlab-ci.yml) to set your `KUBE_CONTEXT`
      1. Set `variables.KUBE_CONTEXT` to point to the registered agent.
      1. Modify the `helmfile.yml` and uncomment what you'd like installed. Example: [helmfile.yml](https://gitlab.com/sandlin/agentk-services/-/blob/groot/helmfile.yaml)
   3. Commit and push the changes. This will trigger a Pipeline execution and install the INGRESS to your cluster.
9. Get the external IP of your INGRESS. Save this value for our project variables.
       ```
       # Make sure you are in the gitlab-managed-apps namespace
       kubens gitlab-managed-apps
       # Get the IP of the ingress.
       kubectl get services -o jsonpath='{..loadBalancer.ingress..ip}' ingress-nginx-ingress-controller
       35.247.53.168
       ```
10. If using a DNS record, you can configure it like:
    ```
     $ gcloud dns record-sets list --zone=papanca
      NAME            TYPE  TTL    DATA
      ...
      *.papanca.com.  A     300    35.247.53.168
    ```

    If you have an A-Record already, the way to reset it is:
    ```
    gcloud dns record-sets delete "*.papanca.com." --type=A --zone=papanca; gcloud dns record-sets create "*.papanca.com." --type=A --zone=papanca --rrdatas=`kubectl get services -o jsonpath='{..loadBalancer.ingress..ip}' ingress-nginx-ingress-controller`
    ```

10. Setup your project's .gitlab-ci.yml file to use the cluster. [example](https://gitlab.com/sandlin/examples/python_flask/-/blob/groot/.gitlab-ci.yml#L8-11)
    - If you are using `nip.io` you can just set your `KUBE_INGRESS_BASE_DOMAIN` in your `.gitlab.ci.yml` to the IP above accordingly. EX: `KUBE_INGRESS_BASE_DOMAIN: '35.247.53.168.nip.io'`

## PREREQ:
---
- [agentk-prereq](sandlin/gcp/gke-agentk/agentk-prereq)
- [agentk-service-account](sandlin/gcp/gke-agentk/agentk-service-account)
- [agentk-network](sandlin/gcp/gke-agentk/agentk-network)
- [agentk-cluster](sandlin/gcp/gke-agentk/agentk-cluster)
- [agentk-cluster-role-binding](sandlin/gcp/gke-agentk/agentk-cluster-role-binding)


## Usage
---
1. Fork this repo
1. Modify terraform.tfvars accordingly.
1. Modify install.sh, setting `GOOGLE_APPLICATION_CREDENTIALS` && `GITLAB_CREDENTIALS`
1. Modify `terraform.backend.http` inside the [main.tf](./main.tf) and replace my project ID with your project ID in the addresses
1. Execute install.sh
   ```
   ./install.sh create
   ```
1. Take note of output. It'll be needed in the next project(s)

    Sample Output:
    ```
    $ ./install.sh create
    ...

    ```
