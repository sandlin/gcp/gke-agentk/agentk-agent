# Your Google Cloud settings
project = "jsandlin-c9fe7132"
region  = "us-west1"
zone    = "us-west1-a"

# Service Account Information - provided via agentk-service-account
cluster_sa_id = "111443113326302267295"
cluster_sa_email = "agentk-guinea-sa@jsandlin-c9fe7132.iam.gserviceaccount.com"


cluster_name = "agentk-terrier"



# The project holding the GitLab Agent configs. 
# For this project, the value is the ID of this project.
gitlab_project_id_agent_config = 35484648 # https://gitlab.com/sandlin/agentk-gcp