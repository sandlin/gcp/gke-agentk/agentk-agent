/*
* Create Network for our TF cluster.
*/
# Store state in the project.
terraform {
  required_version = ">=1.0.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/35484492/terraform/state/groot"
    lock_address   = "https://gitlab.com/api/v4/projects/35484492/terraform/state/groot/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/35484492/terraform/state/groot/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

terraform {
  required_providers {
    kubernetes = "~> 2.3"
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.0"
    }
    graphql = {
      source = "sullivtr/graphql"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~>3.6.0"
    }
  }
}

provider "graphql" {
  url = var.gitlab_graphql_api_url
  headers = {
    "Authorization" = "Bearer ${var.gitlab_token}"
  }
}

provider "gitlab" {
    token = var.gitlab_token
}

provider "google" {
  project     = var.project
}


# Retrieve an access token as the Terraform runner
data "google_client_config" "provider" {}

data "google_container_cluster" "my_cluster" {
  name     = var.cluster_name
  project = var.project
  location = var.region
}


# Get the Service Account Token we will use to control Kubernetes.
data "google_service_account_access_token" "sa_token" {
  provider               = google
  target_service_account = var.cluster_sa_email
  scopes                 = ["cloud-platform"]
  lifetime               = "300s"
}

# Setup provider
provider "kubernetes" {
  host  = "https://${data.google_container_cluster.my_cluster.endpoint}"
  token = data.google_service_account_access_token.sa_token.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.my_cluster.master_auth[0].cluster_ca_certificate,
  )
}


module "gitlab_kubernetes_agent" {
  source = "gitlab.com/nagyv-gitlab/kubernetes-agent-terraform-module/local"
  version = "0.0.6"
  #source = "./modules/kubernetes-agent-terraform-module"
  gitlab_project_id = var.gitlab_project_id_agent_config
  gitlab_graphql_api_url = var.gitlab_graphql_api_url
  agent_name = "gitlab-agent" # https://gitlab.com/sandlin/gitlab_k8s_cluster/k8s-gitlab-agent-config
  agent_namespace = "gitlab-kubernetes-agent" # https://docs.gitlab.com/ee/user/clusters/agent/install/#install-the-agent-into-the-cluster
  agent_version = "stable" # https://docs.gitlab.com/ee/user/clusters/agent/install/#install-the-agent-into-the-cluster
  token_name = "gitlab-agent"
  token_description = "This token is being generated for the gitlab_agent"
  annotations = {}
}
